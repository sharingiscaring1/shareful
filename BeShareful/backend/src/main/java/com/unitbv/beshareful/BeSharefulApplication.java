package com.unitbv.beshareful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeSharefulApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeSharefulApplication.class, args);
	}

}
