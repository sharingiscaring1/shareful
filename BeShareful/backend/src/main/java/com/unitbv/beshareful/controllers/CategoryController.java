package com.unitbv.beshareful.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unitbv.beshareful.entities.Category;
import com.unitbv.beshareful.exceptions.RecordNotFoundException;
import com.unitbv.beshareful.repositories.CategoryRepository;

@RestController
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Category> getAllObjects() {
		return this.categoryRepository.findAll();
	}

	@GetMapping(value = "/category/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Category getObject(@Valid @PathVariable("id") Long id) throws RecordNotFoundException {
		Category categoryOpt = this.categoryRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("[findUser] User not found for this id::" + id.toString()));
		return categoryOpt;
	}

	@PostMapping(value = "/category/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Category createObject(@RequestBody Category category) {
		return this.categoryRepository.save(category);
	}

	@PutMapping(value = "/category/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Category updateObject(@Valid @RequestBody Category category) {
		return this.categoryRepository.save(category);
	}

	@DeleteMapping(value = "category/delete")
	public void deleteObject(@RequestParam("id") Long id) {
		if (this.categoryRepository.existsById(id))
			this.categoryRepository.deleteById(id);
		else
			throw new RecordNotFoundException("[delete] User not found for this id::" + id.toString());
	}
	
	@DeleteMapping(value = "category/delete/{id}")
	public void deleteObjectById(@PathVariable("id") Long id) {
		if (this.categoryRepository.existsById(id))
			this.categoryRepository.deleteById(id);
		else
			throw new RecordNotFoundException("[delete] User not found for this id::" + id.toString());
	}
	
}
