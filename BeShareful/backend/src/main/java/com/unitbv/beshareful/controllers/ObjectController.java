package com.unitbv.beshareful.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unitbv.beshareful.exceptions.RecordNotFoundException;
import com.unitbv.beshareful.repositories.ObjectRepository;
import com.unitbv.beshareful.entities.Object;

@RestController
public class ObjectController {

	@Autowired
	private ObjectRepository objectRepository;
	
	@GetMapping(value = "/objects", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Object> getAllObjects() {
		return this.objectRepository.findAll();
	}

	@GetMapping(value = "/object/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object getObject(@Valid @PathVariable("id") Long id) throws RecordNotFoundException {
		Object objectOpt = this.objectRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("[findUser] User not found for this id::" + id.toString()));
		return objectOpt;
	}

	@PostMapping(value = "/object/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object createObject(@RequestBody Object object) {
		return this.objectRepository.save(object);
	}

	@PutMapping(value = "/object/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object updateObject(@Valid @RequestBody Object object) {
		return this.objectRepository.save(object);
	}

	@DeleteMapping(value = "object/delete")
	public void deleteObject(@RequestParam("id") Long id) {
		if (this.objectRepository.existsById(id))
			this.objectRepository.deleteById(id);
		else
			throw new RecordNotFoundException("[delete] User not found for this id::" + id.toString());
	}
	
	@DeleteMapping(value = "object/delete/{id}")
	public void deleteObjectById(@PathVariable("id") Long id) {
		if (this.objectRepository.existsById(id))
			this.objectRepository.deleteById(id);
		else
			throw new RecordNotFoundException("[delete] User not found for this id::" + id.toString());
	}
	
}
