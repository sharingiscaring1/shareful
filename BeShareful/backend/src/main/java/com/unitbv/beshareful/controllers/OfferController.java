package com.unitbv.beshareful.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unitbv.beshareful.entities.Offer;
import com.unitbv.beshareful.exceptions.RecordNotFoundException;
import com.unitbv.beshareful.repositories.OfferRepository;

@RestController
public class OfferController {
	
	@Autowired
	private OfferRepository offerRepository;
	
	@GetMapping(value = "/offers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Offer> getAllOffers() {
		return this.offerRepository.findAll();
	}

	@GetMapping(value = "/offer/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Offer getOffer(@Valid @PathVariable("id") Long id) throws RecordNotFoundException {
		Offer offerOpt = this.offerRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("[findUser] User not found for this id::" + id.toString()));
		return offerOpt;
	}

	@PostMapping(value = "/offer/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Offer createOffer(@RequestBody Offer offer) {
		return this.offerRepository.save(offer);
	}

	@PutMapping(value = "/offer/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Offer updateOffer(@Valid @RequestBody Offer offer) {
		return this.offerRepository.save(offer);
	}

	@DeleteMapping(value = "/offer/delete")
	public void deleteOffer(@RequestParam("id") Long id) {
		if (this.offerRepository.existsById(id))
			this.offerRepository.deleteById(id);
		else
			throw new RecordNotFoundException("[delete] User not found for this id::" + id.toString());
	}
	
	@DeleteMapping(value = "/offer/delete/{id}")
	public void deleteOfferById(@PathVariable("id") Long id) {
		if (this.offerRepository.existsById(id))
			this.offerRepository.deleteById(id);
		else
			throw new RecordNotFoundException("[delete] User not found for this id::" + id.toString());
	}
}
