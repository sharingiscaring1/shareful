package com.unitbv.beshareful.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unitbv.beshareful.entities.User;
import com.unitbv.beshareful.exceptions.RecordNotFoundException;
import com.unitbv.beshareful.repositories.UserRepository;

@RestController
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	@GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUsers() {
		return this.userRepository.findAll();
	}

	@GetMapping(value = "/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public User getUser(@Valid @PathVariable("id") Long id) throws RecordNotFoundException {
		User usertOpt = this.userRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("[findUser] User not found for this id::" + id.toString()));
		return usertOpt;
	}

	@PostMapping(value = "/user/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public User createUser(@RequestBody User user) {
		return this.userRepository.save(user);
	}

	@PutMapping(value = "/user/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public User updateUser(@Valid @RequestBody User user) {
		return this.userRepository.save(user);
	}

	@DeleteMapping(value = "user/delete")
	public void deleteUser(@RequestParam("id") Long id) {
		if (this.userRepository.existsById(id))
			this.userRepository.deleteById(id);
		else
			throw new RecordNotFoundException("[delete] User not found for this id::" + id.toString());
	}
	@DeleteMapping(value = "user/delete/{id}")
	public void deleteUserById(@PathVariable("id") Long id) {
		if (this.userRepository.existsById(id))
			this.userRepository.deleteById(id);
		else
			throw new RecordNotFoundException("[delete] User not found for this id::" + id.toString());
	}


	
}
