package com.unitbv.beshareful.entities;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.unitbv.beshareful.entities.enums.CategoryType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="categories")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Category {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="category_id",unique=true,nullable = false)
	private Long id;
	
	@Column(name="category_type",nullable = false)
	@Enumerated(EnumType.STRING)
	private CategoryType categoryType;

	/** List of associated objects */
	
	@JsonIgnore
	@ManyToMany(mappedBy = "objectCategories")
	private Set<Object> categoryObjects = new HashSet<>();
	
	

}
