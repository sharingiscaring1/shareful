package com.unitbv.beshareful.entities;

import java.sql.Blob;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.unitbv.beshareful.entities.enums.ObjectState;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name="objects")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Object {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="object_id",unique=true, nullable = false)
	private Long id;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "price", nullable = false)
	private Double price;
	
	@Column(name="state",nullable = false)
	@Enumerated(EnumType.STRING)
	private ObjectState state;
	
	@Column(name = "description")
	private String description;
	
	/**
	 * Put a default picture for nulls. 
	 */
	@Column(name = "picture")
	private Blob picture;
	
	@Column(name = "manufacturer")
	private String manufacturer;
	
	/** Categories set/list */
	@ManyToMany(cascade= {
			CascadeType.PERSIST,
			CascadeType.MERGE
	})
	@JoinTable(name = "objects_categories",
				joinColumns = @JoinColumn(name="object_id"),
				inverseJoinColumns = @JoinColumn(name="category_id"))
	private Set<Category> objectCategories = new HashSet<>();
}
