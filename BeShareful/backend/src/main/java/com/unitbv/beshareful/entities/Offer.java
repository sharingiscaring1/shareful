package com.unitbv.beshareful.entities;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.unitbv.beshareful.entities.enums.OfferStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="offers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Offer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="offer_id",unique=true, nullable = false)
	private Long id;
	
	@Column(name = "quantity", nullable = false)
	private Integer quantity;
	
	@Column(name="status",nullable = false)
	@Enumerated(EnumType.STRING)
	private OfferStatus status;
	
	/**
	 * Unitary price.
	 */
	@Column(name="price",nullable = false)
	private Double price;
	
	@Column(name = "location", nullable = false)
	private String location;
	
	@Column(name = "date_added", nullable = false)
	private Date dateAdded;
	
	@Column(name = "date_deleted")
	private Date dateDeleted;
	
//	@Column(name="trade_id")
//    private Long tradeId;
	
	@Column(name = "offered_object_id", nullable = false)
	private Long objectId;
	
	/** The offering user */
	
	@Column(name = "owner_id", nullable = false)
	private Long ownerId;
	
	/** The subscribers list of users */
}

