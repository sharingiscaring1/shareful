package com.unitbv.beshareful.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.unitbv.beshareful.entities.enums.TradeStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "trades")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Trade {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trade_id", unique = true, nullable = false)
	private Long id;

	@Column(name = "type", nullable = false)
	@Enumerated(EnumType.STRING)
	private TradeStatus status;

	@Column(name = "response")
	private String reason;

	@Column(name = "rating")
	private Double rating;


//	@OneToOne(fetch = FetchType.LAZY,optional = false)
//	@JoinColumn(name="trade_id")
//	private Offer offer;
//	
//	@Column(name="accepted_user")
//	@OneToOne(fetch = FetchType.LAZY)
//	private User acceptedUser;

}
