package com.unitbv.beshareful.entities;

import java.sql.Blob;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.unitbv.beshareful.entities.enums.UserGender;
import com.unitbv.beshareful.entities.enums.UserType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", unique = true, nullable = false)
	private Long id;

	@Column(name = "username", unique = true, nullable = false)
	private String username;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "gender", nullable = false)
	@Enumerated(EnumType.STRING)
	private UserGender gender;

	@Column(name = "phone_number", unique = false, nullable = false)
	private String phoneNumber;

	@Column(name = "profile_picture")
	private Blob profilePicture;

	@Column(name = "location")
	private String location;

	@Column(name = "date_registered", nullable = false)
	private Date dateRegistered;

	@Column(name = "rating")
	private Double rating;

	@Column(name = "type", nullable = false)
	@Enumerated(EnumType.STRING)
	private UserType type;
	
	@JoinColumn(name = "user_metadata_id", unique = true)
    @OneToOne(cascade = CascadeType.ALL)
    private UserMetadata userMetadata;
	
	/** List of associated trades. */

	//private Set<Trade> userTrades = new HashSet<>();
	
	/** List of offers. */
	@OneToMany(mappedBy = "ownerId", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Offer> userOffers = new HashSet<>();
	
}
