package com.unitbv.beshareful.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.unitbv.beshareful.entities.enums.user.metadata.DomainOfWork;
import com.unitbv.beshareful.entities.enums.user.metadata.ProfessionalStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="user_metadata")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserMetadata {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="user_metadata_id", unique=true, nullable = false)
	private Long id;
	
	@Column(name = "professional_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private ProfessionalStatus professionalStatus;
	
	@Column(name = "domain_of_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DomainOfWork domainOfWork;
	
	@Column(name = "net_income", nullable = false)
	private Double netIncome;
	
	@Column(name = "score", nullable = false)
	private Double score;
	
	/** User */
}
