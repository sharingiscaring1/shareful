package com.unitbv.beshareful.entities.enums;

public enum CategoryType {
	BOOKS,
	FILMS,
	ELECTRONICS,
	HOME,
	BEAUTY,
	TOYS,
	SHOES,
	WATCHES,
	CLOTHES,
	SPORTS,
	FOOD
}
