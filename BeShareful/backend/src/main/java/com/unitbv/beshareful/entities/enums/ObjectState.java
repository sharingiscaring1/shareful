package com.unitbv.beshareful.entities.enums;

public enum ObjectState {
	NEW,
	USED,
	OLD,
	DAMAGED
}
