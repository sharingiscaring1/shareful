package com.unitbv.beshareful.entities.enums;

public enum OfferStatus {
	AVAILABLE,
	UNAVAILABLE
}
