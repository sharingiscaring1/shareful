package com.unitbv.beshareful.entities.enums;

public enum TradeStatus {
	POSTED,
	WAITING,
	APPROVED,
	DENIED
}
