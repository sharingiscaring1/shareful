package com.unitbv.beshareful.entities.enums;

public enum UserGender {
	MALE,
	FEMALE,
	UNDEFINED
}
