package com.unitbv.beshareful.entities.enums;

public enum UserType {
	ADMIN,
	REGULAR
}
