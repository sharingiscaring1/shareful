package com.unitbv.beshareful.entities.enums.user.metadata;

public enum ProfessionalStatus {
	STUDENT,
	UNEMPLOYED,
	EMPLOYED,
	RETIRED,
	RECENTLY_UNEMPLOYED
}
