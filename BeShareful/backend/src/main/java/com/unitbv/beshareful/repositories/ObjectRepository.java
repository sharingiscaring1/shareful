package com.unitbv.beshareful.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unitbv.beshareful.entities.Object;

@Repository
public interface ObjectRepository extends JpaRepository<Object, Long> {

}
