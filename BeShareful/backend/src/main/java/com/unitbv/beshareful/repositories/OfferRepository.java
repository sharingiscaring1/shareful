package com.unitbv.beshareful.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unitbv.beshareful.entities.Offer;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {

}
