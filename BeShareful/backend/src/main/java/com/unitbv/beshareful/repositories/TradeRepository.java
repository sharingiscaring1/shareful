package com.unitbv.beshareful.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unitbv.beshareful.entities.Trade;

@Repository
public interface TradeRepository extends JpaRepository<Trade, Long> {

}
