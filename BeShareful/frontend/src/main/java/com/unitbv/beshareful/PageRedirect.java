package com.unitbv.beshareful;

import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class PageRedirect implements WebMvcConfigurer {
	@Override
	  public void addViewControllers(ViewControllerRegistry registry) {
	    registry.addViewController("/").setViewName("forward:/main.xhtml");
	    registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	  }
}
