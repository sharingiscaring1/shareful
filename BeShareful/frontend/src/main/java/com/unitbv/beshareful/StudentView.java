package com.unitbv.beshareful;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import lombok.Getter;

@Named(value = "studentView")
@ViewScoped
@Getter
public class StudentView {

	List<Student> student = new ArrayList<Student>();
	
	@PostConstruct
	public void init() {
		student.add(new Student("Sergiu", "031031201"));
		student.add(new Student("Silviu", "031031205"));
		student.add(new Student("Test1", "031391211"));
		student.add(new Student("Test2", "031031251"));
		student.add(new Student("Test3", "03231201"));
	}
	
}
