﻿# Unused objects sharing / donations
## Base functionality
The application will be able to register and handle user accounts. 
What properties will have an account?
- username (used for login)
- email (used for login)
- first name
- last name 
- gender (null)
- picture (null)
- location
- phone number
- date registered
- password (hashed + salt)
- rating 
- a list of objects posted for trading 
- type (regular/admin/intermediate)

What can a user do? The user can authenticate. Once authenticated, the user will have a **dashboard**.
On the dashboard the user can do the following:
- search for items (search in all the object's fields if the searched word is a valid word
	-  filter by category
- he will be able to see the last 10 added products/what's in trending (by object rating)
- posting of an object (add title, etc. --> Post entity).

If the user is an intermediate, then he will be able only to intermediate trades and he can also take actions on users/posts.

The app will have posts. Posts can contain the following:
- quantity
- trade status (status Type - we'll think more about this : posted, traded etc.)
- overall price
- the user who trades the object
- location of the object (which, in most cases, is the same as the user's location, to be handled in the back-end)
- date added
- date deleted (just for us)
- soft delete
- list of users that have submitted for the object(s)
- object_id

The application will also have (of course) objects. What will an object contain?
- title
- price (0 by default)
- object state (level of wear)
- description 
- picture
- manufacturer (string) 
-  [...]

Main technologies to be used: Java Spring, Primefaces, Hibernate/EclipseLink.

# Goals
 - The user can create an account and can authenticate.
 - The user can create an offer.
 - The user can also edit/delete an offer
	 - After maximum 3 edits, the user can still undo the edits he's made.
 - The user can see all the offers posted by others.
 - The user can see all the trades he has been involved in (approved/waiting/rejected - include a reason).
	 - If approved, the requester will be able to see the other person's contact data. If the user opts for standard delivery, then he will also be able to see updates on the object's delivery in real time.
 - The user can request a trade for an offer.
  - The user can review only the object and the trader and only after trading.
 - The user can search for an offer 
	 - by plain string
	 - by category (filtering)
 - The user can see a history of all his trades.
 - The admin can delete offers.
 - The admin can penalize users (account lock, temporary ban).
 - The admin can mediate trades. (select the most appropriate user to get the object)
	 -  How? By gathering data using a form. What could possibly a form contain?
				 -  social status (married/engaged/has children)
				 - profession (salaried/unemployed/student)
				 - domain of work (if is salaried)
				 - net income
				 - monthly absolute necessary expenses 
	After gathering this data, a score will be computed and will be further used when deciding which person should receive that object. For now, we'll suppose that people who register are well intended.
	If rejected, an user will receive a reason about it. Also, the admin will not be able to see the users' names (to go fully unbiased).
	


